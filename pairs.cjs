function pairs(obj) {
    if(typeof obj !== "object"|| obj === null){
        return [];
    }
    let newObject=[];
    for (let keys in obj){
        newObject.push([keys , obj[keys]]);
    }
    return newObject;
}
    
module.exports=pairs;

