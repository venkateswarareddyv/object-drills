function defaults(obj, defaultProps){
    if(typeof obj !== "object" || obj === null){
        return [];
    }
    for(let item in defaultProps){
        if(!item in obj){
            obj[item]=defaultProps[item];
            
        }else{
            obj[item]=defaultProps[item]
        }
    }
    return obj;
}

module.exports=defaults;
