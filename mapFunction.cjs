function mapObject(obj,callBackForProblem3) {
    
    if(typeof obj !== "object" || obj === null){
        return [];
    }
    let  newObject={};
    for (let keys in obj){
        let newValue=callBackForProblem3(obj[keys],2);
        newObject[keys]=newValue;
    }
    return newObject;
}

module.exports =mapObject;

