function values(obj) {
    if(typeof obj !== "object" || obj === null){
        return [];
    }
    let newObject=[];
    for (let keys in obj){
        newObject.push(obj[keys]);
    }
    return newObject;
}

module.exports= values;
