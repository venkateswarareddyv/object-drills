function invert(obj) {
    if(typeof obj !== "object" || obj === null){
        return [];
    }
    let newObject={};
    for (let keys in obj){
        newObject[obj[keys]]=keys;
    }
    return newObject;
}

module.exports=invert;
