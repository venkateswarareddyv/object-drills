function keys(obj) {
    if(typeof obj !== "object" || obj === null){
        return [];
    }
    let newObject=[]
    for (let keys in obj){
        newObject.push(keys);
    }
    return newObject;

}

module.exports = keys;
